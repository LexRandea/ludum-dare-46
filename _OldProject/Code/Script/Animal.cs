using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animal : MonoBehaviour
{
    [SerializeField] private float speed;

    [SerializeField] private float topBoundX = 42.0f;
    [SerializeField] private float lowerBoundX = -42.0f;
    [SerializeField] private float topBoundZ = 25.0f;
    [SerializeField] private float lowerBoundZ = -25.0f;

    // Start is called before the first frame update
    void Start()
    {
        this.transform.RotateAround(this.transform.position, Vector3.up, Random.Range(0, 360));
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position += this.transform.forward * speed * Time.deltaTime;


        if (transform.position.x > topBoundX || transform.position.x < lowerBoundX)
        {

            //destroy it out of the bounds
            Destroy(gameObject);
            //Overrides: apply all met de prefabs
        }
        else if (transform.position.z > topBoundZ || transform.position.z < lowerBoundZ)
        {
            //Debug.Log("Game Over!");
            Destroy(gameObject);

        }
    }
}