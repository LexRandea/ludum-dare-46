﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{

    private Rigidbody rb;
    private AudioSource playerAudio;
    public AudioClip goalMade;


    // Start is called before the first frame update
    void Start()
    {
        this.rb = this.GetComponent<Rigidbody>();
        playerAudio = GetComponent<AudioSource>();
    }


    // handle ball interactions on tigger enter
    private void OnTriggerEnter(Collider other)
    {
        // check if we hit a ball pick up trigger
        var pickupTrigger = other.GetComponent<BallPickUp>();
        if (pickupTrigger != null)
        {
            pickupTrigger.PickUpball(this);
        }

        // check if we hit a score trigger
        var goal= other.GetComponent<Goal>();
        if (goal != null)
        {
            goal.oppositeTeam.score += 1;
            //Audio making a goal
            playerAudio.PlayOneShot(goalMade);
        }
    }

    

    // make ik possible to set physics state of ball directly
    public void SetKinematic(bool state)
    {
        this.rb.isKinematic = state;
    }


}
