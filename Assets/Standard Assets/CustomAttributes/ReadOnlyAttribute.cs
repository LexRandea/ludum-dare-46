﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Put this script in the Editor folder
// source : https://www.patrykgalach.com/2020/01/20/readonly-attribute-in-unity-editor/

/// <summary>
/// Read Only attribute.
/// Attribute is used to mark ReadOnly properties.
/// </summary>
public class ReadOnlyAttribute : PropertyAttribute { }