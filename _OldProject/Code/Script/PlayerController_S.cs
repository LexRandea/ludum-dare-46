﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private SphereCollider playerRb;

    //draw game
    public bool gameOver;

    //meerdere powerups
    public GameObject powerupIndicator; //meerdere gameObjects

    public int powerUpDuration = 5;
    private float powerupStrength = 25; // how hard to hit enemy with powerup
    private float spawnRangeX = 10;
    private float spawnRangeZ = 10;
    private float startDelay = 2;
    private float spawnInterval = 7;

    private bool hasPowerup = false;

    //Audio variables*********


    //Animation
    private Animator playerAnim;


    //private AudioSource playerAudio;
    //public AudioClip jumpSound;
    // public AudioClip crashSound;


    // Start is called before the first frame update
    void Start()
    {
        playerRb = GetComponent<SphereCollider>();

        InvokeRepeating("SpawnRandomPowerup", startDelay, spawnInterval);

        playerAnim = GetComponent<Animator>();

        // Audio
        //playerAudio = GetComponent<AudioSource>();

    }

    //Random powerindicator plaats
    // Update is called once per frame
    void Update()
    {

        float xPos = Random.Range(-spawnRangeX, spawnRangeX);
        float zPos = Random.Range(spawnRangeZ, -spawnRangeZ);

        powerupIndicator.transform.position = transform.position + new Vector3(xPos, 0, zPos);


    }
    //Random powerups in het veld brengen: door muisklik
    

    //timerfunctie

    IEnumerator PowerupCooldown()
    {
        yield return new WaitForSeconds(powerUpDuration);
        hasPowerup = false;
        powerupIndicator.SetActive(false);
    }

    //Powerup: speler van andere ploeg verwijderen af het veld
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Powerup"))
        {
            hasPowerup = true;
            Destroy(other.gameObject);
            StartCoroutine(PowerUpCountDownRoutine());
            powerupIndicator.SetActive(true);
        }
    }

    //7 seconden tot de powerup weer weg is/niet meer actief is

    IEnumerator PowerUpCountDownRoutine()
    {
        yield return new WaitForSeconds(7);
        hasPowerup = false;
        powerupIndicator.SetActive(false);

    }



}



