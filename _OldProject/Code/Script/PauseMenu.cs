﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{	
	public static bool GameIsPaused = true;

	public GameObject Menu;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
		{
			if (GameIsPaused)
			{
				Resume();
				
			} else
			{ 	
				Pause();
				
			}
		}
    }
	
	public void Pause()
	{
		Menu.SetActive(true);
		Time.timeScale = 0f;
		GameIsPaused = true;
		//Setting volume to 0%
		AudioListener.volume = 0.0f;
	}
	
	public void Resume()
	{
		Menu.SetActive(false);
		Time.timeScale = 1f;
		GameIsPaused = false;
		//Setting volume back to 100%
		AudioListener.volume = 1.0f;

	}
	
	public void Settings()
	{
		Debug.Log("Opening settings...");
	}	
	
	public void Exit() {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit ();
#endif
	}
}
