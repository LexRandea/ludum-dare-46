﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Team : MonoBehaviour
{
    public int score;
    public string teamName;
    public Transform ownGoal;
    public Transform targetGoal;
    public Transform ball;
    private List<Transform> players;


    // Start is called before the first frame update
    void Start()
    {
        // find all players in team
        players = new List<Transform>();
        foreach (Transform child in transform)
        {
            // add player to list
            players.Add(child);

            // set player to team
            child.GetComponent<Player>().SetTeam(this);
        }

    }


    // get list of all players
    public List<Transform> GetPlayers()
    {
        return players;
    }


    // return true if team has ball
    public bool HasBall()
    {
        foreach (Transform player in players)
        {
            if (player.GetComponent<Player>().HasBall())
            {
                return true;
            }
        }
        return false;
    }

}
