﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
     // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKey)
        {
            
        }
    }

    Vector3 GetLookDirection(Vector3 target)
    {
        Vector3 res = target - transform.position;
        res.Normalize();
        res = Vector3.RotateTowards(transform.forward, res, 0.1f, 10.0f);
        res.y = 0;
        res.Normalize();
        return res;
    }

    public void Shoot()
    {
       print("Shoot");
       
       //team.ball.GetComponent<Ball>().Kick(this.transform);
    }
}
