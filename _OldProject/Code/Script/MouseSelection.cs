﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseSelection : MonoBehaviour
{

    [SerializeField] private Material highlightMaterial;
    [SerializeField] private Transform cursor;
    [SerializeField] private LayerMask selectables;
    [SerializeField] private int maxDistance = 1000;
    [SerializeField] private float dropHeight = 8;
    private string objectName = null;
	[SerializeField] private PreventDeselectionGroup deselectionGroup;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (!IsPointerOverUIObject() && (!string.IsNullOrEmpty(objectName)))
            {
                if (Physics.Raycast(ray, out hit, maxDistance, selectables))
                {
                    GameObject obj = Instantiate(Resources.Load(objectName)) as GameObject;
                    objectName = null;
                    obj.transform.position = hit.point + Vector3.up * dropHeight;
                    obj.transform.RotateAround(obj.transform.position, Vector3.up, Random.Range(0, 360));
                    obj.SetActive(true);
					deselectionGroup.Deselect();
                }
            }
        }
    }
	
	private bool IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }

    public void GenerateObjectName(string objectName)
    {
        this.objectName = "Prefabs/GodPowers/" + objectName;
    }
	
    public void ChangeTime()
    {
		if (Time.timeScale == 1f)
			Time.timeScale = Random.Range(0f, 4f);
		else Time.timeScale = 1f;
    }
	
}
