﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallPickUp : MonoBehaviour
{
    public Player player;


    // called by ball when he enters the ballpickup trigger
    public void PickUpball(Ball ball)
    {
        if (!player.HasBall())
        {
            player.ConnectBall(ball);
        }
    }
}
