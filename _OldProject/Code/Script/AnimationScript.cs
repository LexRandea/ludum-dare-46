﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationScript : MonoBehaviour
{

    private Animator playerAnim;    //component 



    // Start is called before the first frame update
    void Start()
    {
        playerAnim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
            //Example
            //playerAnim.SetTrigger("throwing_trig");
            //playerAnim.SetBool(throwing_b, true);
            playerAnim.SetBool("standing_b", true); //boolean
            playerAnim.SetTrigger("standing_trig");
        //playerAnim.SetBool("static_b", true);//trigger

    }
}
