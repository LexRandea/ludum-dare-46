﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PreventDeselectionGroup : MonoBehaviour
{
    private EventSystem evt;
    [SerializeField] private GameObject sel;

    private void Start()
    {
        evt = EventSystem.current;
    }

    private void Update()
    {
        if (evt.currentSelectedGameObject != null && evt.currentSelectedGameObject != sel)
            sel = evt.currentSelectedGameObject;
        else if (sel != null && evt.currentSelectedGameObject == null)
            evt.SetSelectedGameObject(sel);
    }
	
	public void Deselect()
	{
		sel = null;
		evt.SetSelectedGameObject(sel);
	}
}