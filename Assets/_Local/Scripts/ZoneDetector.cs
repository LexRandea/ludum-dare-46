﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class ZoneDetector : MonoBehaviour
{
    // This scripts keeps track on which field zone its entity is located by updating the fieldzone OntriggerEnter

    [ReadOnlyAttribute]
    public string currentZone;

    private void Start()
    {
        this.gameObject.layer = LayerMask.NameToLayer("FieldZone");
    }

    private void OnTriggerEnter(Collider other)
    {
        this.currentZone = other.transform.gameObject.name;
    }
}
