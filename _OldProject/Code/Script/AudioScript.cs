﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioScript : MonoBehaviour
{
    // Start is called before the first frame update

    private AudioSource playerAudio;
    public AudioClip powerupIndicator;
    public AudioClip supporterTeam1;
    public AudioClip supporterTeam2;
    public AudioClip whistleRefereeGod;

    private bool pausing = false;


    void Start()
    {
        playerAudio = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {
        //playerAudio.PlayOneShot(powerupIndicator, 1.0f);
        //playerAudio.PlayOneShot(supporterTeam1, 15.0f);
        //playerAudio.PlayOneShot(whistleRefereeGod, 5.0f);
        //playerAudio.PlayOneShot(supporterTeam2, 15.0f);

        //Sound Mute Button

        if (Input.GetKeyDown(KeyCode.Space))
        {

            AudioListener.volume = 0.0f;
        }
        else if (Input.GetKeyDown(KeyCode.A))
        {
            AudioListener.volume = 0.5f;
            Debug.Log("a key pressed");
        }
        else if (Input.GetKeyDown(KeyCode.C))
        {
            playerAudio.Play();
        }

        else if (Input.GetKeyDown(KeyCode.B))
        {
            playerAudio.Pause();
        }


       // if (Input.GetButton("Mute"))
        //{
          //  playerAudio.Play();
        //}

    }


}