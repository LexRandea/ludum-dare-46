﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    // exposed private variables
    [SerializeField] int speed;
    [SerializeField] int rotationalSpeed;
    [SerializeField] Transform model;
    [SerializeField] bool updateAngularSpeed;

    // used to rotate the ball model
    private float angularSpeed;

    // Start is called before the first frame update
    void Start()
    {
        computeAngularSpeed();
    }

    void computeAngularSpeed()
    {
        // derive angular speed
        float radius = this.transform.lossyScale.x * 0.5f;
        angularSpeed = speed * 360.0f / (2.0f * Mathf.PI * radius);
    }


    // Update is called once per frame
    void Update()
    {

        if (updateAngularSpeed)
        {
            computeAngularSpeed();
        }

        // Get generic input directions
        float dt = Time.deltaTime;
        float translation = Input.GetAxis("Vertical") * speed * dt;
        float rotation = Input.GetAxis("Horizontal") * rotationalSpeed * dt;
        float ballRotation = Input.GetAxis("Vertical") * this.angularSpeed * dt;

        // enforce that ball can only rotate and turn when moving
        if (Mathf.Abs(translation) > 1e-3)
        {
            // Move translation along the object's z-axis
            this.transform.Translate(0, 0, translation);

            // Rotate around our y-axis
            this.transform.Rotate(0, rotation, 0);

            // Rotate ball model around x-axis (local red axis)
            model.transform.Rotate(ballRotation, 0.0f, 0.0f);
        }

    }
}
