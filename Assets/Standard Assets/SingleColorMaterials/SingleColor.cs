﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Make sure that the material SingleColor is placed at Assets/Resources/Materials/SingleColor !
// This script allows to change the color of a mesh when it used the basic unity shader
// ExecuteAlways ensures that the chosen color can be seen in the editor

// This way no single color materials have to be created
[ExecuteAlways]

// This way no single color materials have to be created by the user
[AddComponentMenu("MyScripts/SingleColor")]

public class SingleColor : MonoBehaviour
{

    [SerializeField]
    private Color myColor = Color.black;
    private MaterialPropertyBlock propBlock;
    private Renderer rend;


    // Start is called before the first frame update
    private void Awake()
    {
        propBlock = new MaterialPropertyBlock();
        rend = GetComponent<Renderer>();
        if (rend != null)
            rend.material = Resources.Load<Material>("Materials/SingleColor");
    }

    // This function ensures that change to color are updated in the editor
    public void Update()
    {
        if (rend != null)
        {

            // get property block
            rend.GetPropertyBlock(propBlock);

            // Apply selected color
            propBlock.SetColor("_Color", myColor);

            // Apply the edited values to the renderer.
            rend.SetPropertyBlock(propBlock);
        }
    }

}
