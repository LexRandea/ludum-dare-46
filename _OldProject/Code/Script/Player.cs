﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Player : MonoBehaviour
{
    [SerializeField] private Team team;
    [SerializeField] private bool isKeeper;
    [SerializeField] private Transform ballPosition;
    [SerializeField] private NavMeshAgent agent;
    [SerializeField] private Animator anim;

    [SerializeField] private int speedWithBall = 15;
    [SerializeField] private int speedWithoutBall = 20;
    [SerializeField] private int shootingPower = 10;
    [SerializeField] private int shootingDistance = 20;
    [SerializeField] private int passingPower = 10;
    [SerializeField] private int passingDistance = 20;
    [SerializeField] private Ball ball;

    private Vector3 startPosition;

    // Start is called before the first frame update
    void Start()
    {
        startPosition = this.transform.position;
        agent.speed = speedWithoutBall;
    }


    // Update is called once per frame
    void Update()
    {
        TakeAction();
    }


    // update position and rotatin
    void UpdateMovement(Vector3 target)
    {
        agent.SetDestination(target);
        transform.rotation = Quaternion.LookRotation(GetLookDirection(target));
    }


    // determines the actions of player
    void TakeAction()
    {

        var noise = new Vector3(Random.Range(-1, 1), 0, Random.Range(-1, 1));
        noise.Normalize();

        noise = new Vector3(0, 0, 0);

        // if we are a keeper
        if (isKeeper)
        {
            // we are the keeper
            // and we have the ball
            if (HasBall())
            {
                // run towards closest player
                UpdateMovement(GetClosestTeamPlayer().position + noise);

                // try to pass
                TryToPass();

            }
            else
            {
                // we don't have the ball

                // if team has the ball
                if (team.HasBall())
                {
                    // relax around starting position)
                    UpdateMovement(startPosition + 5.0f * noise);
                }
                else
                {
                    // move towards closest ball
                    UpdateMovement(GetClosestBall().position);
                }
            }
        }
        else
        {
            // we are not the keeper
            // and we have the ball
            if (HasBall())
            {
                // run towards target goal
                UpdateMovement(team.targetGoal.position + noise * 10.0f);

                // try to shoot
                TryToShoot();

                // try to pass
                TryToPass();
            }
            else
            {
                // if the team has the ball
                if (team.HasBall())
                {
                    // run toward the target goal
                    var pos = team.targetGoal.position;
                    pos.x = transform.position.x;
                    UpdateMovement(pos + noise * 5.0f);
                }
                else
                {
                    // if the team has not the ball
                    // try to find a ball 
                    var ball = GetClosestBall();
                    UpdateMovement(ball.position);
                    
                }
            }

        }


    }


    // get tranform of closest ball
    Transform GetClosestBall()
    {
        var balls = GameObject.FindGameObjectsWithTag("Ball");

        float dist = 0.0f;
        float minDist = 10000.0f;
        Transform closest = null;

        foreach(GameObject ball in balls)
        {
            dist = Vector3.Distance(ball.transform.position, this.transform.position);
            if (dist < minDist)
            {
                minDist = dist;
                closest = ball.transform;

            }
        }

        return closest;
    }


    // find closest player in team
    Transform GetClosestTeamPlayer()
    {
        {
            var players = team.GetPlayers();

            float dist = 0.0f;
            float minDist = 10000.0f;
            Transform closest = null;

            foreach (Transform player in players)
            {
                dist = Vector3.Distance(player.position, this.transform.position);
                if (dist < minDist && player.transform != this.transform)
                {
                    minDist = dist;
                    closest = player.transform;

                }
            }

            return closest;
        }
    }
    

    // determines the direction in which the player will look
    Vector3 GetLookDirection(Vector3 target)
    {
        Vector3 res = target - transform.position;
        res.Normalize();
        res = Vector3.RotateTowards(transform.forward, res, 0.1f, 10.0f);
        res.y = 0;
        res.Normalize();
        return res;
    }


    // try to shoot at target goal
    void TryToShoot()
    {
        // check if we have the ball
        if (HasBall())
        {
            var towardGoal = team.targetGoal.position - ball.transform.position;

            // check if we are close enough to attempt shooting
            if (towardGoal.magnitude < shootingDistance)
            {
                // shoot ray to check if shot is clear
                RaycastHit hit;
                Ray ray = new Ray(ball.transform.position, towardGoal);
                if (Physics.Raycast(ray, out hit))
                {
                    // if shot is clear (ray hit goal)
                    if (hit.collider.tag == "Goal")
                    {
                        // shoot at goal
                        ShootBall(towardGoal, shootingPower);
                    }

                }
            }
        }
    }


    // try to pass at closest player
    void TryToPass()
    {
        // check if we have the ball
        if (HasBall())
        {
            // get closest teamplayer
            var closest = GetClosestTeamPlayer();

            // check if we are close enough to attempt shooting
            float dist = Vector3.Distance(transform.position, closest.position);
            if (dist < passingDistance)
            {
                
                // shoot ray to check if pass is clear
                RaycastHit hit;
                Ray ray = new Ray(transform.position, closest.position-transform.position);
                int layer_mask = LayerMask.GetMask("Player");
                if (Physics.Raycast(ray, out hit, 1000, layer_mask))
                {
                    print(hit.transform.gameObject.name);
                    Player player = hit.transform.GetComponent<Player>();
                    // if shot is clear (ray hit target player)
                    if (player != null)
                    {
                        print("ray hits player");
                        if (player.team == team && !player.isKeeper)
                        {
                            var towardPlayer = player.transform.position - this.ballPosition.position;
                            ShootBall(towardPlayer, shootingPower);
                        }
                    }

                }
            }

        }
    }


    // returns true if player has a ball
    public bool HasBall()
    {
        return ball != null;
    }


    // connect bal to to this player
    public void ConnectBall(Ball ball)
    {
        // disconnect ball from other player
        if (ball.transform.parent != null)
        {
            var otherPlayer = ball.transform.parent.parent.GetComponent<Player>();
            otherPlayer.LoseBall();
        }

        // connect ball to this player
        GetBall(ball);
    }


    // set speeds on getting the ball
    public void GetBall(Ball ball)
    {
        ball.SetKinematic(true);
        ball.transform.SetParent(this.ballPosition.transform);
        ball.transform.position = ballPosition.position;
        agent.speed = speedWithBall;
        this.ball = ball;
    }


    // set speed on loosing the ball
    public void LoseBall()
    {
        ball.transform.SetParent(null);
        this.ball = null;
        agent.speed = speedWithoutBall;
    }


    // give an impluse to the ball
    public void ShootBall(Vector3 direction, float power)
    {
        Rigidbody rb = ball.GetComponent<Rigidbody>();
        LoseBall();
        rb.isKinematic = false;
        rb.AddForce(direction.normalized * power, ForceMode.Impulse);
    }


    public void SetTeam(Team myTeam)
    {
        this.team = myTeam;
    }

    // draw path
    void OnDrawGizmosSelected()
    {

        var nav = GetComponent<NavMeshAgent>();
        if (nav == null || nav.path == null)
            return;

        var line = this.GetComponent<LineRenderer>();
        if (line == null)
        {
            line = this.gameObject.AddComponent<LineRenderer>();
            line.material = new Material(Shader.Find("Sprites/Default")) { color = Color.yellow };
            line.SetWidth(0.5f, 0.5f);
            line.SetColors(Color.yellow, Color.yellow);
        }

        var path = nav.path;

        line.SetVertexCount(path.corners.Length);

        for (int i = 0; i < path.corners.Length; i++)
        {
            line.SetPosition(i, path.corners[i]);
        }

    }
}
